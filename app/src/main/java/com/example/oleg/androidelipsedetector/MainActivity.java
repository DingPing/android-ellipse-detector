package com.example.oleg.androidelipsedetector;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    public static final int CAMERA_PERMISSION_RESULT_ID = 1;
    private JavaCameraView mOpenCvCameraView;

    private boolean cameraPermissionGranted = false;
    private boolean cameraInitialize = false;

    float density;
    int thresh = 100;
    int maxThresh = 250;

    int _foundLeftX   = 235;
    int _foundLeftY   = 630;
    int _foundRightX  = 875;
    int _foundRightY  = 630;
    int _foundWidth   = 150;
    int _foundHeight  = 150;
    //int ellipseMax    = 1;
    int addit         = 0;

    int foundLeftX    = _foundLeftX;
    int foundLeftY    = _foundLeftY;
    int foundRightX   = _foundRightX;
    int foundRightY   = _foundRightY;
    int foundWidth    = _foundWidth;
    int foundHeight   = _foundHeight;

    SeekBar threshView;
    SeekBar threshMaxView;
    TextView threshValue;
    TextView threshMaxValue;

    List<Scalar> scalars = new ArrayList<>();

    public static boolean checkPermission(Context context, String permission_name) {
        return ((Build.VERSION.SDK_INT < 23) || (ContextCompat.checkSelfPermission(context, permission_name) == PackageManager.PERMISSION_GRANTED));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        density = getResources().getDisplayMetrics().density;
        //ellipseMax *= density;

        scalars.add(new Scalar(255, 10, 20));
        scalars.add(new Scalar(10, 255, 20));
        scalars.add(new Scalar(20, 10, 255));

        threshView     = (SeekBar)  findViewById(R.id.thresh);
        threshMaxView  = (SeekBar)  findViewById(R.id.threshMax);
        threshValue    = (TextView) findViewById(R.id.threshValue);
        threshMaxValue = (TextView) findViewById(R.id.threshMaxValue);

        threshValue.setText(String.valueOf(thresh));
        threshMaxValue.setText(String.valueOf(addit));
        threshView.setProgress(thresh);
        threshMaxView.setProgress(addit);
        threshView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                threshValue.setText(String.valueOf(i));
                thresh = i;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        threshMaxView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                threshMaxValue.setText(String.valueOf(i));
                addit = i;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        if (checkPermission(this, Manifest.permission.CAMERA)) {
            cameraPermissionGranted = true;
            enableCamera();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MainActivity.CAMERA_PERMISSION_RESULT_ID);
        }
    }

    private void enableCamera() {
        if (cameraPermissionGranted) {
            View view = findViewById(R.id.view);
            mOpenCvCameraView = (JavaCameraView) view;
            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
            if (!cameraInitialize) {
                mOpenCvCameraView.enableView();
                cameraInitialize = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null ) {
            cameraInitialize = false;
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initDebug();
        enableCamera();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    private Mat whiteBalance(Mat img, int addit) {
        Mat result = new Mat();
        img.copyTo(result);
        Mat tmp = new Mat();
        result.convertTo(tmp, CvType.CV_64FC3);
        int size = (int) (tmp.total() * tmp.channels());
        double[] d = new double[size];
        tmp.get(0, 0, d);
        for (int k = 0; k < size; k++) {
            d[k] += addit;
            if (d[k] < 0) {
                d[k] = 0;
            } else if (d[k] > 255) {
                d[k] = 255;
            }
        }
        result.put(0, 0, d);
        return result;
    }

    class DetectBestEllipse {
        Rect mid  = null;
        Rect max  = null;
        RotatedRect midR = null;
        RotatedRect maxR = null;

        public void add(RotatedRect rotatedRect, Rect rect) {
            if (max == null || (max.height < rect.height && max.width < rect.width)) {
                max = rect;
                maxR = rotatedRect;
            }
            if (mid == null || (rect.height > mid.height && rect.width > mid.width &&
                    rect.height < max.height && rect.width < max.width)) {
                mid = rect;
                midR = rotatedRect;
            }
        }

        public RotatedRect getMidR() {
            return midR;
        }

        public RotatedRect getMaxR() {
            return maxR;
        }
    }

    public Mat detect(Mat img) {
        Mat leftImg = new Mat(img, new Rect(foundLeftX, foundLeftY, foundWidth, foundHeight));
        Mat rightImg = new Mat(img, new Rect(foundRightX, foundRightY, foundWidth, foundHeight));


        Imgproc.cvtColor(leftImg, leftImg, Imgproc.COLOR_RGB2GRAY);
        Imgproc.cvtColor(rightImg, rightImg, Imgproc.COLOR_RGB2GRAY);

        Imgproc.GaussianBlur(leftImg, leftImg, new Size(9, 9), 2, 2);
        Imgproc.GaussianBlur(rightImg, rightImg, new Size(9, 9), 2, 2);

        Mat circles = new Mat();
        Imgproc.HoughCircles(leftImg, circles, Imgproc.CV_HOUGH_GRADIENT,
                foundWidth / 16, // accumulator resolution
                foundWidth, // minimum distance between two circles
                thresh, // minimum number of votes
                200, // minimum number of votes
                foundWidth / 4, foundWidth / 2 // min and max radius
        );
        for (int i = 0; i < circles.cols(); i++) {
            double vec[] = circles.get(0, i);
            if (vec == null) {
                continue;
            }

            Point center = new Point(vec[0] + foundLeftX, vec[1] + foundLeftY);
            //Point center = new Point(vec[0], vec[1]);
            int radius = (int) Math.round(vec[2]);
            Imgproc.circle(img, center, 3, new Scalar(0, 255, 255), (int) -density, 8, 0);
            Imgproc.circle(img, center, radius, new Scalar(255, 0, 0), (int) density, 8, 0);
        }

        Imgproc.HoughCircles(rightImg, circles, Imgproc.CV_HOUGH_GRADIENT,
                foundWidth / 16, // accumulator resolution
                foundWidth, // minimum distance between two circles
                thresh, // minimum number of votes
                200, // minimum number of votes
                foundWidth / 4, foundWidth / 2 // min and max radius
        );
        for (int i = 0; i < circles.cols(); i++) {
            double vec[] = circles.get(0, i);
            if (vec == null) {
                continue;
            }

            Point center = new Point(vec[0] + foundRightX, vec[1] + foundRightY);
            //Point center = new Point(vec[0], vec[1]);
            int radius = (int) Math.round(vec[2]);
            Imgproc.circle(img, center, 3, new Scalar(0, 255, 255), (int) -density, 8, 0);
            Imgproc.circle(img, center, radius, new Scalar(255, 0, 0), (int) density, 8, 0);
        }



        Imgproc.rectangle(img, new Point(foundLeftX, foundLeftY), new Point(foundLeftX + foundWidth, foundLeftY + foundHeight), new Scalar(0, 0, 255), (int) density);
        Imgproc.rectangle(img, new Point(foundRightX, foundRightY), new Point(foundRightX + foundWidth, foundRightY + foundHeight), new Scalar(0, 0, 255), (int) density);
        return  img;
    }

    public Mat detect1(Mat img) {
        Imgproc.cvtColor(img,  img,  Imgproc.COLOR_RGBA2GRAY);
        Imgproc.threshold(img, img, thresh, maxThresh, Imgproc.THRESH_TOZERO_INV);
        Mat tmp = new Mat();
        img.copyTo(tmp);
        Mat leftImg = new Mat(tmp, new Rect(foundLeftX, foundLeftY, foundWidth, foundHeight));
        //Mat rightImg = new Mat(img, new Rect(foundRightX, foundRightY, foundWidth, foundHeight));

        DetectBestEllipse leftBestEllipse = new DetectBestEllipse();

        Mat hierarchy;
        List<MatOfPoint> objects;

        List<Point> leftArea  = new ArrayList<>();
        List<Point> rightArea = new ArrayList<>();

        //Imgproc.cvtColor(leftImg,  leftImg,  Imgproc.COLOR_RGBA2GRAY);
        //Imgproc.cvtColor(rightImg, rightImg, Imgproc.COLOR_RGBA2GRAY);

        //leftImg  = whiteBalance(leftImg, addit);
        //rightImg = whiteBalance(leftImg, addit);
        //rightImg = whiteBalance(rightImg);

        //Imgproc.threshold(leftImg,  leftImg,  thresh, maxThresh, Imgproc.THRESH_TOZERO_INV);
        //Imgproc.threshold(rightImg, rightImg, thresh, maxThresh, Imgproc.THRESH_TOZERO_INV);

        //left area found
        objects = new ArrayList<>();
        hierarchy = new Mat();
        Imgproc.findContours(leftImg, objects, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

        Imgproc.cvtColor(img, img, Imgproc.COLOR_GRAY2RGB);
        for (int i = 0; i < objects.size(); i++) {
            MatOfPoint object = objects.get(i);
            List<Point> points = object.toList();
            if (points.size() > 5) {
                double minX = points.get(0).x;
                double maxX = points.get(0).x;
                double minY = points.get(0).y;
                double maxY = points.get(0).y;
                for (int j = 0; j < points.size(); j++) {
                    Point p1 = points.get(j);
                    if (minX > p1.x) {
                        minX = p1.x;
                    }
                    if (maxX < p1.x) {
                        maxX = p1.x;
                    }
                    if (minY > p1.y) {
                        minY = p1.y;
                    }
                    if (maxY < p1.y) {
                        maxY = p1.y;
                    }
                }
                double width  = maxX - minX;
                double height = maxY - minY;
                if (width < foundWidth / 6 || height < foundWidth / 6) continue;
                for (int j = 0; j < points.size() - 1; j++) {
                    Point p1 = points.get(j);
                    Point p2 = points.get(j + 1);
                    Imgproc.line(img, new Point(p1.x + foundLeftX, p1.y + foundLeftY), new Point(p2.x + foundLeftX, p2.y + foundLeftY), scalars.get(i % scalars.size()), (int) density);
                }
                /*for (int j = 0; j < points.size(); j++) {
                    points.get(j).x += foundLeftX;
                    points.get(j).y += foundLeftY;
                }

                Mat m = Converters.vector_Point2f_to_Mat(points);
                RotatedRect box = Imgproc.fitEllipse(new MatOfPoint2f(m));
                Rect rect = box.boundingRect();
                int checkHeight = (int) ((double) rect.width / 4.5);
                int checkWidth  = (int) ((double) rect.height / 4.5);
                if (rect.height - checkHeight < rect.width && rect.height + checkHeight > rect.width &&
                         rect.width - checkWidth < rect.height && rect.width + checkWidth > rect.width) {
                    leftBestEllipse.add(box, rect);
                    //Imgproc.ellipse(img, box, scalars.get(i % scalars.size()), (int) density, 8);
                }
                /*for (int j = 0; j < points.size(); j++) {
                    Point p = points.get(j);
                    leftArea.add(new Point(p.x + foundLeftX, p.y + foundLeftY));
                }*/
            }
        }

        if (leftBestEllipse.getMaxR() != null) {
            Imgproc.ellipse(img, leftBestEllipse.getMaxR(), new Scalar(255, 0, 0), (int) density, 8);
        }
        if (leftBestEllipse.getMidR() != null) {
            Imgproc.ellipse(img, leftBestEllipse.getMidR(), new Scalar(0, 255, 0), (int) density, 8);
        }


        //right area found
        /*objects = new ArrayList<>();
        hierarchy = new Mat();
        Imgproc.findContours(rightImg, objects, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
        for (int i = 0; i < objects.size(); i++) {
            MatOfPoint object = objects.get(i);
            List<Point> points = object.toList();
            if (points.size() > 0) {
                for (int j = 0; j < points.size(); j++) {
                    Point p = points.get(j);
                    rightArea.add(new Point(p.x + foundRightX, p.y + foundRightY));
                }
            }
        }*/
        /*if (leftArea.size() > 5) {
            Mat m = Converters.vector_Point2f_to_Mat(leftArea);
            RotatedRect box = Imgproc.fitEllipse(new MatOfPoint2f(m));
            Rect rect = box.boundingRect();
            int checkHeight = (int) ((double) rect.width / 4.5);
            int checkWidth  = (int) ((double) rect.height / 4.5);
            if (rect.height - checkHeight < rect.width && rect.height + checkHeight > rect.width &&
                    rect.width - checkWidth < rect.height && rect.width + checkWidth > rect.width) {
                Imgproc.ellipse(img, box, new Scalar(255, 0, 0), (int) density, 8);
            }
        }*/
        /*if (rightArea.size() > 5) {
            Mat m = Converters.vector_Point2f_to_Mat(rightArea);
            RotatedRect box = Imgproc.fitEllipse(new MatOfPoint2f(m));
            Rect rect = box.boundingRect();
            int checkHeight = (int) ((double) rect.width / 4.5);
            int checkWidth  = (int) ((double) rect.height / 4.5);
            if (rect.height - checkHeight < rect.width && rect.height + checkHeight > rect.width &&
                    rect.width - checkWidth < rect.height && rect.width + checkWidth > rect.width) {
                Imgproc.ellipse(img, box, new Scalar(255, 0, 0), (int) density, 8);
            }
        }*/
        Imgproc.rectangle(img, new Point(foundLeftX, foundLeftY), new Point(foundLeftX + foundWidth, foundLeftY + foundHeight), new Scalar(0, 0, 255), (int) density);
        //Imgproc.rectangle(img, new Point(foundRightX, foundRightY), new Point(foundRightX + foundWidth, foundRightY + foundHeight), new Scalar(0, 0, 255), (int) density, (int) density);
        return img;
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        return detect(inputFrame.rgba());
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int grantedCount = 0;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                grantedCount += 1;
            }
        }
        boolean isGranted = permissions.length == grantedCount;

        cameraPermissionGranted = isGranted;
        if (isGranted) {
            if (requestCode == CAMERA_PERMISSION_RESULT_ID) {
                enableCamera();
            } else {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA }, MainActivity.CAMERA_PERMISSION_RESULT_ID);
            }
        }
    }
}
